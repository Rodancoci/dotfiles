#!/usr/bin/bash

hour=$(date +"%H")
day=$(date +"%u")

if [[ $day -ge "1" ]] && [[ $day -le "5" ]] ; then
	if [[ $hour -ge "9" ]] && [[ $hour -le "15" ]] ; then
		/usr/bin/slack -u &>/dev/null & disown
	fi
fi

