#
# ~/.bashrc
#

# If not running interactively, don't do anything
#[[ $- != *i* ]] && return

stty -ixon

[[ -f ~/.bashrc.aliases ]] && . ~/.bashrc.aliases
[[ -f ~/.bashrc.functions ]] && . ~/.bashrc.functions
[[ -f /etc/profile.d/vte.sh ]] && . /etc/profile.d/vte.sh

export STATUS_BACKEND=AppIndicator

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"

eval "$(pyenv init --path)"

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh

stty -ixoff

#parse_git_branch() {
#     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
#}
#PS1='\[\e[97m\]\t \[\e[m\][\u@\h \W]\[\e[93m\]$(parse_git_branch)\[\e[m\] \$ '

if [[ $LANG = '' ]]; then
	export LANG=en_US.UTF-8
fi

EDITOR=/usr/bin/vim
XDG_CONFIG_HOME=$HOME/.config

export PATH="$PATH:$HOME/.scripts:$HOME/.local/bin"
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
export DOTNET_CLI_TELEMETRY_OPTOUT="1"
export QT_QPA_PLATFORMTHEME="qt5ct"
export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/docker.sock"

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
