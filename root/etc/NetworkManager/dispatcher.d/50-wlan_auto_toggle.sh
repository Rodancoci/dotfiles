#!/bin/sh

WLAN_STATUS_FILE="/etc/wlan_last_status"

case $1 in
    enp0s20u1 | enp0s20u2 | enp0s20u3)
        case "$2" in
            up)
                if [ $(nmcli radio wifi) = "disabled" ]; then
                    last_status="off"
                else
                    last_status="on"
                fi
                echo $last_status > $WLAN_STATUS_FILE
                nmcli radio wifi off
                ;;
            down)
                if [ -f $WLAN_STATUS_FILE ]; then
                    last_status=$(<$WLAN_STATUS_FILE)
                else
                    last_status="on"
	        fi
                if [ $last_status = "on" ]; then
                    nmcli radio wifi on
                fi
                ;;
        esac
    ;;
    *)
        if [ -f $WLAN_STATUS_FILE ]; then
            last_status=$(<$WLAN_STATUS_FILE)
        else
            last_status="on"
        fi
        if [ $last_status = "on" ]; then
            nmcli radio wifi on
        fi 
    ;;
esac
